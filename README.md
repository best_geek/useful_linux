# README #

Collection of useful linux stuff

#### make ssl certs ####

```
#!/bin/bash
openssl req -x509 \
 -nodes -days 365 -newkey rsa:4096 \
 -keyout website.key \
 -out website.pem \
 -subj "/C=UK/ST=UK/L=London/CN=servername.localhost/emailAddress=admin@domain.net"
```

#### usermod ####
add to docker group
```sudo usermod -aG docker nathan``` 

specific gid and uids
```useradd -u 1001 -g 1001 nathan```

#### escape bad chars ####
```
#!/bin/bash

#simple test to escape special chars


RUNWITH="$@"


#note, have removed / as this is can be where logfile lives
if (echo $RUNWITH | grep -q "[]:?#@\!\$&'()*+,;%[]")
then
        echo "STOP!"
        echo "Remote user called wrapper with $RUNWITH and was stopped" >> /var/websible/websible.log
fi
```

#### get parent pid ####
```bash -c 'echo $$; do command'```

#### get arguments in shell scripts ####
```
#get sysargs
for i in "$@"
do
case "$i" in
    -p=*|--playbook=*)
    PLAYBOOK="${i#*=}"
    shift # past argument=value
    ;;
    -rl=*|--run-log=*)
    RUNLOG="${i#*=}"
    shift # past argument=value
    ;;
    -s=*|--stop-playbook=*)
    STOPPLAYBOOK="${i#*=}"
    shift # past argument=value
    ;;

esac
done
````

#### hide a crontab ####
place in /etc/cron.d/
then call with crontab /etc/cron.d/filename
make sure filename has a empty line at the end for a valid cron


#### replace sendmail command with something else ####
edit /etc/mail.rc
adding this would make it ssmtp: ```set sendmail="/usr/bin/msmtp"```

#### template for assinging a static interface ####
```
iface ens192 inet static
        address 10.148.0.4
        netmask 255.255.255.0
        network 10.148.0.0
        broadcast 192.168.0.255
        gateway 10.148.0.1
        dns-nameservers 10.148.0.10
```

#### get underlying os name ####
```cat /etc/os-release | grep "PRETTY" ```

#### run as root? ####
```
#ROOT USER CHECK
if [ "$EUID" -ne 0 ]
        then echo "Please run as root"
        exit 1
fi
````

#### set a custom path including the default ####
```export PATH=/newpath:$PATH ```

#### set no pwd prompt for users in /etc/sudoers ####
```<user>  ALL=NOPASSWD: ALL```
should use visudo for this. Never know what has a handle on sudoers. Though guess you could do lsof /etc/sudoers

#### SSH config file magic ####
set up passwordless auth only:
```
Match User rbackup
        AuthenticationMethods publickey
```

public key OR 2fa:
```
#default settings to include google authenticator
AuthenticationMethods publickey keyboard-interactive
```

allow bouncing ports through ssh server
```GatewayPorts yes```

